﻿<?php session_start();
require_once('db.php');
logiran(3);
?>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>
<?php
if(!isset($_GET['a'])) { $a = ''; } else { $a = $_GET['a']; }
$c = db();
echo '<p align="right"><a href="logout.php">ODJAVA</a></p>';
switch($a) 
{
	case 'create': unos(); break;
	case 'update': izmjeni(); break;
	case 'delete': brisi(); break;
	default : pregled(); form(); // READ
}

function pregled(){
	// ISPIS KATEGORIJA SA EDIT I DELETE LINKOVIMA
	global $c;
	$sql = "SELECT id, naziv FROM kategorije";
	$r = $c->query($sql);
	while($row = $r->fetch_assoc())
	{
	  echo '<p>'.$row['naziv'];
	  echo ' - <a href="?a=update&id='.$row['id'].'">IZMJENI</a>';
	  echo ' - <a href="?a=delete&id='.$row['id'].'">IZBRISI</a>';
	  echo '</p>';
	}
}
function form(){
	// OBRAZAC SA JEDNIM TEXT POLJEM
	global $c;
	echo '<form method="post" action="?a=create">';
	echo '<input type="text" name="naziv">';
	echo '<input type="submit" name="submit" value="Spremi">';
	echo '</form>';
}
function unos(){
	global $c;
	$naziv = $_POST['naziv'];
	$sql = "INSERT INTO kategorije(naziv) VALUES ('$naziv')";
	$c->query($sql);
	echo '<a href="administratori.php">Povratak</a>';
}
function izmjeni()
{	
	global $c;
	$id = $_GET['id']; // POSLANO SA PREGLEDA "?a=update&id=11"
	if(!$_POST)
	{
		$sql = "SELECT naziv FROM kategorije WHERE id=$id";
		$r = $c->query($sql);
		$row = $r->fetch_assoc(); // Netreba while jer je samo 1 redak
		// OBRAZAC SA JEDNIM TEXT POLJEM (POPUNJENO POLJE)
		echo '<form method="post" action="?a=update&id='.$id.'">';
		echo '<input type="text" name="naziv" value="'.$row['naziv'].'">';
		echo '<input type="submit" name="submit" value="Spremi">';
		echo '</form>';
	}
	else
	{
		// UPDATE U BAZI
		$naziv = $_POST['naziv'];
		$sql = "UPDATE kategorije SET naziv='$naziv' WHERE id=$id";
		$c->query($sql);
		echo '<a href="administratori.php">Povratak</a>';
	}
}
function brisi(){
	// NEMA POTVRDE, ODMAH DELETE, NEMA OBRAZAC
	global $c;
	$id = $_GET['id'];
	$sql = "DELETE FROM kategorije WHERE id=$id LIMIT 1";
	$c->query($sql);
	echo '<a href="administratori.php">Povratak</a>';
}
?>
</body>
</html>

-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 22, 2014 at 02:00 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `world`
--

-- --------------------------------------------------------

--
-- Table structure for table `clanci`
--

CREATE TABLE IF NOT EXISTS `clanci` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `naslov` varchar(100) NOT NULL,
  `vk_autora` int(5) unsigned NOT NULL,
  `vk_kategorije` int(5) unsigned NOT NULL,
  `datum` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `objavljen` tinyint(1) unsigned NOT NULL,
  `uvod` text NOT NULL,
  `tekst` text NOT NULL,
  `pogledi` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `clanci`
--

INSERT INTO `clanci` (`id`, `naslov`, `vk_autora`, `vk_kategorije`, `datum`, `objavljen`, `uvod`, `tekst`, `pogledi`) VALUES
(1, 'OOP u PHP', 1, 1, '2014-04-10 07:11:27', 1, 'Objektno programiranje u PHP-u', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pretium quam quis elit molestie, at sollicitudin arcu pretium. Pellentesque vel sem eu libero ullamcorper vestibulum quis a metus. Vestibulum dignissim magna ac mi sagittis blandit. Suspendisse mattis lectus leo, hendrerit imperdiet leo tristique eget. Nulla laoreet sem ac facilisis facilisis. Duis in massa vitae sem auctor suscipit pharetra id nisi. Ut at nulla non justo feugiat ornare. Integer non nisl accumsan, rhoncus nulla id, aliquet dui. Nullam interdum accumsan ligula, vel lobortis nisl sodales eu. Donec aliquet ullamcorper bibendum. Maecenas scelerisque urna sed condimentum blandit. Quisque arcu tellus, commodo quis dignissim eget, sollicitudin eget enim. Mauris pellentesque mi et tempus auctor.\r\n\r\nQuisque pharetra purus ut dui gravida volutpat. Donec luctus aliquam molestie. Etiam ac nunc et ligula faucibus tincidunt eu vel augue. Maecenas ac massa neque. Quisque sapien augue, blandit id aliquet vel, malesuada et nisl. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi id dictum ipsum. Donec nec massa nisl. Nullam a velit lobortis purus pulvinar tincidunt. Donec at massa sodales, malesuada arcu ut, lobortis turpis. Cras purus felis, cursus at nunc nec, gravida venenatis magna. Morbi vitae elementum mauris, id egestas mauris. Pellentesque tempus sem et diam adipiscing consequat. ', 211),
(2, 'Uvod u HTML5', 1, 2, '2014-04-22 12:34:53', 1, 'Blah blah', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pretium quam quis elit molestie, at sollicitudin arcu pretium. Pellentesque vel sem eu libero ullamcorper vestibulum quis a metus. Vestibulum dignissim magna ac mi sagittis blandit. Suspendisse mattis lectus leo, hendrerit imperdiet leo tristique eget. Nulla laoreet sem ac facilisis facilisis. Duis in massa vitae sem auctor suscipit pharetra id nisi. Ut at nulla non justo feugiat ornare. Integer non nisl accumsan, rhoncus nulla id, aliquet dui. Nullam interdum accumsan ligula, vel lobortis nisl sodales eu. Donec aliquet ullamcorper bibendum. Maecenas scelerisque urna sed condimentum blandit. Quisque arcu tellus, commodo quis dignissim eget, sollicitudin eget enim. Mauris pellentesque mi et tempus auctor.\r\n\r\nQuisque pharetra purus ut dui gravida volutpat. Donec luctus aliquam molestie. Etiam ac nunc et ligula faucibus tincidunt eu vel augue. Maecenas ac massa neque. Quisque sapien augue, blandit id aliquet vel, malesuada et nisl. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi id dictum ipsum. Donec nec massa nisl. Nullam a velit lobortis purus pulvinar tincidunt. Donec at massa sodales, malesuada arcu ut, lobortis turpis. Cras purus felis, cursus at nunc nec, gravida venenatis magna. Morbi vitae elementum mauris, id egestas mauris. Pellentesque tempus sem et diam adipiscing consequat. ', 3);

-- --------------------------------------------------------

--
-- Table structure for table `kategorije`
--

CREATE TABLE IF NOT EXISTS `kategorije` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `naziv` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `kategorije`
--

INSERT INTO `kategorije` (`id`, `naziv`) VALUES
(1, 'PHP'),
(2, 'HTML5');

-- --------------------------------------------------------

--
-- Table structure for table `korisnici`
--

CREATE TABLE IF NOT EXISTS `korisnici` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `ime` varchar(30) NOT NULL,
  `prezime` varchar(40) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` char(40) NOT NULL,
  `vk_tip` int(5) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `korisnici`
--

INSERT INTO `korisnici` (`id`, `ime`, `prezime`, `username`, `password`, `vk_tip`) VALUES
(1, 'Igor', 'Jugo', 'ijugo', '202cb962ac59075b964b07152d234b70', 3),
(2, 'Igor', 'Jugo', 'ijugo1', '202cb962ac59075b964b07152d234b70', 1),
(3, 'Igor', 'Jugo', 'ijugo2', '202cb962ac59075b964b07152d234b70', 2),
(4, 'Igor', 'Jugo', 'ijugo3', '202cb962ac59075b964b07152d234b70', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tip_korisnika`
--

CREATE TABLE IF NOT EXISTS `tip_korisnika` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `naziv` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tip_korisnika`
--

INSERT INTO `tip_korisnika` (`id`, `naziv`) VALUES
(1, 'Autori'),
(2, 'Urednici'),
(3, 'Administratori');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

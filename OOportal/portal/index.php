<?php require('admin/db.php'); $c = db(); ?>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style type="text/css">
    body {
        font-family: Arial;
        font-size: 12px;
    }
    p.info {
        background-color:#ccc;
        font-size: 10px;
        padding: 10px;
    }
</style>
</head>

<body>
<table width="75%" border="1" cellspacing="0" cellpadding="10">
  <tr> 
    <td colspan="2">ZAGLAVLJE</td>
  </tr>
  <tr> 
    <td width="22%" rowspan="2" valign="top"><p>KATEGORIJE</p>
<?php
echo '<p><a href="'.$_SERVER['SCRIPT_NAME'].'">POČETNA</a></p>';
$sql = "SELECT * FROM kategorije";
$r = $c->query($sql);
while($row = $r->fetch_assoc())
{
	echo '<p>';
	echo '<a href="?a=kat&id='.$row['id'].'">'.$row['naziv'].'</a>';
	echo '</p>';
}
?>
	  
	  </td>
          <td width="78%" height="41">Sortiraj po:  <a href="?a=sortiraj&kako=vrijeme">Vremenu objave (Najnoviji)</a> - <a href="?a=sortiraj&kako=pogledi">Broju pogleda (Najpopularniji)</a></td>
  </tr>
  <tr> 
    <td height="342" valign="top">
<?php
/*
 * GLAVNI SADRZAJ
 * PREMA PARAMETRU a U GET NIZU ODLUCUJEMO STO PRIKAZATI
 */
if(!isset($_GET['a'])) { $a = ''; } else { $a = $_GET['a']; }
switch($a){
    case 'kat': ispisClanakaUKategoriji(); break;
    case 'clanak': ispisJednogClanka(); break;
    case 'sortiraj': postaviSortiranje(); break;
    default: ispisClanaka(); break;
}

function ispisClanaka(){
    global $c;

$sql = "SELECT c.id, c.naslov, c.uvod, DATE_FORMAT(c.datum,'%d.%m.%Y u %H:%i') AS datum, c.pogledi, a.ime, a.prezime, k.naziv 
FROM clanci c 
LEFT JOIN kategorije k ON (c.vk_kategorije = k.id) 
LEFT JOIN korisnici a ON (c.vk_autora = a.id) 
AND c.objavljen = 1";
if(isset($_COOKIE['kako'])){
    if($_COOKIE['kako']=='vrijeme') { $orderby = 'c.datum'; } else { $orderby = 'c.pogledi'; }
    $sql.=" ORDER BY $orderby DESC";
}
$r = $c->query($sql);
    while($row = $r->fetch_assoc())
    {
            echo '<p>';
            echo '<strong>'.$row['naslov'].'</strong></p>';
            echo '<p>'.$row['uvod'].'</p>';
            echo '<p><a href="?a=clanak&id='.$row['id'].'">više...</a>';
            echo '<p class="info">'
            . 'Objavljeno: '.$row['datum'].
                    ' - Pogleda:'.$row['pogledi'].
                    ' - Autor: '.$row['ime'].' '.$row['prezime'].
                    ' - Kategorija: '.$row['naziv'].'</p>';
            
    }
}

function ispisClanakaUKategoriji(){
    global $c;
    $kategorija = $_GET['id'];
    $sql = "SELECT c.id, c.naslov, c.uvod, DATE_FORMAT(c.datum,'%d.%m.%Y u %H:%i') AS datum, c.pogledi, a.ime, a.prezime, k.naziv 
FROM clanci c 
LEFT JOIN kategorije k ON (c.vk_kategorije = k.id) 
LEFT JOIN korisnici a ON (c.vk_autora = a.id) 
WHERE k.id = $kategorija 
AND c.objavljen = 1";
if(isset($_COOKIE['kako'])){
    if($_COOKIE['kako']=='vrijeme') { $orderby = 'c.datum'; } else { $orderby = 'c.pogledi'; }
    $sql.=" ORDER BY $orderby DESC";
}
    $r = $c->query($sql);
    while($row = $r->fetch_assoc())
    {
            echo '<h2>Kategorija: '.$row['naziv'].'</h2>';
            echo '<p>';
            echo '<strong>'.$row['naslov'].'</strong></p>';
            echo '<p>'.$row['uvod'].'</p>';
            echo '<p><a href="?a=clanak&id='.$row['id'].'">više...</a>';
            echo '<p class="info">'
            . 'Objavljeno: '.$row['datum'].
                    ' - Pogleda:'.$row['pogledi'].
                    ' - Autor: '.$row['ime'].' '.$row['prezime'].
                    ' - Kategorija: '.$row['naziv'].'</p>';
    }
}

function ispisJednogClanka(){
    global $c;
    $id = $_GET['id'];
    $sql = "SELECT * FROM clanci WHERE id = $id AND objavljen = 1 LIMIT 1";
    $r = $c->query($sql);
    $row = $r->fetch_assoc();
    
    echo '<p>';
    echo '<strong>'.$row['naslov'].'</strong></p>';
    echo '<p>'.$row['uvod'].'</p>';
    echo '<p>'.$row['tekst'].'</p>';
    echo '<p><a href="'.$_SERVER['SCRIPT_NAME'].'">natrag...</a>';
    echo '<p>Objavljeno: '.date('d.m.Y \u H:i',strtotime($row['datum'])).' Pogleda:'.$row['pogledi'].'</p>';
    echo '<hr>';
    
    inkrementirajBrojPrikaza($id);
    
}

function inkrementirajBrojPrikaza($id){
    global $c;
    $sql = "UPDATE clanci SET pogledi = pogledi+1 WHERE id = $id";
    $c->query($sql);
}

function postaviSortiranje(){
    $kako = $_GET['kako']; // DOHVATI NACIN SORTIRANJA IZ LINKA
    if(isset($_COOKIE['kako'])){ // AKO JE VEC POSTAVLJEN
       if($_COOKIE['kako']!=$kako){ setcookie('kako', $kako, time()+360000); } // POSTAVLJEN JE, ALI NA DRUGI NACIN SORTIRAJA, POSTAVI NA NOVI NACIN
    }
    else { // INACE, POSTAVI GA
        setcookie('kako', $kako, time()+360000);
    }
    
    echo '<p>Postavljeno sortiranje....<a href="'.$_SERVER['SCRIPT_NAME'].'">Početna</a></p>';
}
?>
   </td>
  </tr>
  <tr> 
    <td colspan="2" align="right">
        <a href="admin/">ADMINISTRACIJA</a></span>
        
    </td>
  </tr>
</table>
</body>
</html>

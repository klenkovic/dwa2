﻿<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>

<p>ZADACI ZA VJEZBU</p>
<p>1. Napravite funkciju na temelju funkcije select() koja će omogućiti ispis 
  padajuće liste sa grupama (vidi kod padajuće liste ispod)</p>

<p><select name="proba">
<optgroup label="Administratori">
<option>Ante</option>
<option>Ivo</option>
<option>Ana</option>
</optgroup>
<optgroup label="Programeri">
<option>Maja</option>
<option>Ivan</option>
<option>Bero</option>
</optgroup>
</select>

<p>Prijedlog: umjesto običnog polja kao treći argument pošaljite 2D polje (polje 
  polja)</p>
<p>Prijedlog / pitanje: može li se problem riješiti na način da se umjesto slanja 
  2D polja doda novi argument funkciji?</p>
<p>2. Napravite funkciju na temelju funkcije select() koja dobiva dodatni argument 
  u koji se unosi stupac/atribut u tablici koji sadrži vrijednosti vanjskog ključa 
  po kojem treba napraviti grupiranje u padajućoj listi.</p>
<p>Npr.:</p>
<p>korisnici(id, ime, prezime, vk_tip)<br>
  -------------------------------------<br>
  1 Ante Antic 1<br>
  2 Ana Anic 2<br>
  3 Pero Peric 1<br>
  4 Mara Maric 1</p>
<p>vrste(id, naziv)<br>
  ----------------<br>
  1 Student<br>
  2 Profesor</p>
<p>Funkciji će se pri pozivu poslati naziv atributa &quot;vk_tip&quot; i prema 
  njemu će se ispisati padajuća lista sljedećeg oblilka</p>
<form name="form1" method="post" action="">
  <select name="select">
  <optgroup label="Student">
    <option value="1">Ante Antic</option>
    <option value="4">Mara Maric</option>
    <option value="3">Pero Peric</option>
  </optgroup>
  <optgroup label="Profesor">
    
	<option value="2">Ana Anic</option>
	</optgroup>
  </select>
</form>
<p>&nbsp;</p>
</body>
</html>

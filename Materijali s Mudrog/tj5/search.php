﻿<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>
<?php
require 'admin/db.php';
if(!$_POST)
{
	echo '<form method="post" action="">';
	echo 'Unesite pojam: ';
	echo '<input type="text" name="pojam">';
	echo '<input type="submit" value="Kreni">';
	echo '</form>';
}
else
{
	$pojam = $_POST['pojam'];
	$c = db();
	// UPITI
	$sqlCity = "SELECT ct.Name, ct.Population, ct.District,  
				c.Name AS CountryName FROM City ct, Country c 
				WHERE ct.Name LIKE '%$pojam%' 
				AND ct.CountryCode = c.Code ";
	
	$sqlCountryLang = "SELECT * FROM CountryLanguage 
					   WHERE Language LIKE '%$pojam%'";
					   
	$sqlCountry = "SELECT * FROM Country WHERE Name LIKE '%$pojam%'";
	// DOHVATI PODATKE
	$rCity = $c->query($sqlCity);
	$rCL = $c->query($sqlCountryLang);
	$rCountry = $c->query($sqlCountry);
	// KOLIKO IMA REDAKA?
	$brCity = $rCity->num_rows;
	$brCL = $rCL->num_rows;
	$brCountry = $rCountry->num_rows;
	// ISPISI AKO IMA REDAKA
	if($brCity>0)
	{
		echo '<h1>Gradovi</h1>';
		while($red=$rCity->fetch_assoc())
		{
			// Name, Population, District
			echo '<p>'.$red['Name'].' '.$red['Population'].' '.$red['District'].' '.$red['CountryName'].'</p>';
		}
	}
	if($brCL>0)
	{
		echo '<h1>Jezici</h1>';
		while($red=$rCL->fetch_assoc())
		{
		//  Language, IsOfficial, Percentage
		echo '<p>'.$red['Language'].' '.$red['IsOfficial'].' '.$red['Percentage'].'</p>';
		}
	}
	if($brCountry>0)
	{
		echo '<h1>Zemlje</h1>';
		// Name, Population, Continent
		while($red=$rCountry->fetch_assoc())
		{
		
		echo '<p>'.$red['Name'].' '.$red['Population'].' '.$red['Continent'].'</p>';
		}
	}
	
}
?>
</body>
</html>

﻿<?php session_start();
require_once('db.php');
logiran();
?>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<?php
if(!isset($_GET['a'])) { $a = ''; } else { $a = $_GET['a']; }
$c = db();
switch($a) 
{
	case 'unos': unos(); break;
	case 'izmjena': izmjeni(); break;
	case 'potvrdi': potvrdi(); break;
	case 'brisi': brisi(); break;
	default : pregled();
} 

function brisi()
{
	global $c;
	$zemlja = $_GET['id'];
	// IZRAZ ZA BRISANJE GRADOVA
	$dCity = "DELETE FROM City WHERE CountryCode = '$zemlja'";// UPIT
	$c->query($dCity);// IZVRSI
	$br = $c->affected_rows;// DOHVATI affected_rows
	echo '<p>Izbrisano '. $br.' gradova</p>';// ISPIŠI
	// IZRAZ ZA BRISANJE JEZIKA
	$dCl = "DELETE FROM CountryLanguage WHERE CountryCode = '$zemlja'";// UPIT
	$c->query($dCl);// IZVRSI
	$br = $c->affected_rows;// DOHVATI affected_rows
	echo '<p>Izbrisano '. $br.' jezika</p>';// ISPIŠI
	// IZRAZ ZA IZBRISATI ZEMLJU
	$dCl = "DELETE FROM Country WHERE Code = '$zemlja'";// UPIT
	$c->query($dCl);// IZVRSI
	$br = $c->affected_rows;// DOHVATI affected_rows
	echo '<p>Izbrisana '. $br.' zemlja</p>';// ISPIŠI
	
	echo '<h1><a href="admin.php">Pregled</a></h1>';
}

function potvrdi()
{
	global $c;
	$zemlja = $_GET['id'];
	$ime = $_GET['ime'];
	echo 'Zelite brisati zemlju: '. $ime.'?';
	// PROVJERI IMA LI TA ZEMLJA GRADOVA?
	$sqlCity = "SELECT * FROM City WHERE CountryCode = '$zemlja'";
	$r = $c->query($sqlCity);
	if($r->num_rows>0)
	{
		echo '<h1>Moram izbrisati i '. $r->num_rows.' gradova</h1>';
		echo '<ol>';
		while($red = $r->fetch_assoc())
		{
			echo '<li>'.$red['Name'].'</li>';
		}
		echo '</ol>';
	}
	// PROVJERI IMA LI TA ZEMLJA JEZIKA
	$sqlCity = "SELECT * FROM CountryLanguage WHERE CountryCode = '$zemlja'";
	$r = $c->query($sqlCity);
	if($r->num_rows>0)
	{
		echo '<h1>Moram izbrisati i '. $r->num_rows.' jezika</h1>';
		echo '<ol>';
		while($red = $r->fetch_assoc())
		{
			echo '<li>'.$red['Language'].'</li>';
		}
		echo '</ol>';
	}
	
	echo '<h1><a href="?a=brisi&id='.$zemlja.'">
				POTVRDI BRISANJE</a></h1>';
	
}


function pregled()
{
	global $c;
	$sql = "SELECT Code, Name FROM Country";
	$r = $c->query($sql);
	echo '<table border="1" cellpadding="4">';
	while($red = $r->fetch_assoc())
	{
		echo '<tr><td>'.$red['Name'].'</td>
		<td>
		<a href="?a=potvrdi&ime='.$red['Name'].'&id='.$red['Code'].'">BRIŠI</a>
		</td></tr>';
	}
	echo '</table>';
}
?>
</body>
</html>

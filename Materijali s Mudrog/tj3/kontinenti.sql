-- phpMyAdmin SQL Dump
-- version 3.3.2deb1ubuntu1
-- http://www.phpmyadmin.net
--
-- Računalo: localhost
-- Vrijeme generiranja: Ožu 20, 2014 u 05:14 PM
-- Verzija poslužitelja: 5.1.62
-- PHP verzija: 5.3.2-1ubuntu4.15

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza podataka: `dwa2_jjokic`
--

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `kontinenti`
--

CREATE TABLE IF NOT EXISTS `kontinenti` (
  `id` int(1) unsigned NOT NULL AUTO_INCREMENT,
  `naziv` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Izbacivanje podataka za tablicu `kontinenti`
--

INSERT INTO `kontinenti` (`id`, `naziv`) VALUES
(1, 'Asia'),
(2, 'Europe'),
(3, 'North America'),
(4, 'Africa'),
(5, 'Oceania'),
(6, 'Antarctica'),
(7, 'South America');

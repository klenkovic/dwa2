<?php

require('admin/db.php');
$c = db();
	
?>

<html>
<head>
<title>Pretraživač</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
    <h1>Pretraživač</h1>

    <?php
    
    if(!$_POST){
	echo "<form method=\"post\" action=\"\">";
	echo "Odaberite pojam:<br />";
	echo "<input type=\"text\" name=\"pojam\" />";
	echo "<input type=\"submit\" value=\"Kreni\">";
	echo "</form>";
    }else{
	
	$pojam = $_POST['pojam'];
	
	$sqlCity = "SELECT City.*, County.Name AS cName FROM City,Country WHERE Name LIKE '%".$pojam."%' AND City.CountyCode = Country.Code;";
	$sqlCountryLang = "SELECT * FROM CountryLanguage WHERE Language LIKE '%".$pojam."%';";
	$sqlCountry = "SELECT * FROM Country WHERE Name LIKE '%".$pojam."%';";
	
	$rCity = $c->query($sqlCity);
	$rCountryLang = $c->query($sqlCountryLang);
	$rCountry = $c->query($sqlCountry);
	
	$brCity = $rCity->num_rows;
	$brCountryLang = $rCountryLang->num_rows;
	$brCountry = $rCountry->num_rows;
	
	if($brCity > 0){
	    echo "<h2>Gradovi</h2>";
	    while($red = $rCity->fetch_assoc()){
		echo "Ime: ".$red["Name"]."<br />Broj stanovnika: ".$red["Population"]."<br />District: ".$red["District"]."<br />Država: ".$red["cName"]."<br />";
		echo "<br />";
	    }
	    echo "<br />";
	}
	if($brCountryLang > 0){
	    echo "<h2>Jezici</h2>";
	    while($red = $rCountryLang->fetch_assoc()){
		echo "Ime jezika: ".$red["Language"]."<br />Je oficijalan: ".$red["IsOfficial"]."<br />Postotak: ".$red["Percentage"]."<br />";
		echo "<br />";
	    }
	    echo "<br />";
	}
	if($brCountry > 0){
	    echo "<h2>Kontinenti</h2>";
	    while($red = $rCountry->fetch_assoc()){
		echo "Ime: ".$red["Name"]."<br />Populacija: ".$red["Population"]."<br />Kontinent: ".$red["Continent"]."<br />";
		echo "<br />";
	    }
	    echo "<br />";
	}
	
    }
    
    
    ?>
    
</body>
</html>